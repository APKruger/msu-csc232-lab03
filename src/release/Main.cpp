/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Main.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          <FILL ME IN ACCORDINGLY>
 * @brief   Entry point to this application.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <random>

/**
 * @brief Create a new type that has semantic value for array indexes.
 */
using array_index = int;

/**
 * @brief This function prints the address of the given value.
 * @param value An integer whose address shall be printed
 * @post  The address of the given value is printed in hexadecimal notation
 *        while the actual parameter remains unchanged.
 */
void printAddress(int value);

/**
 * @brief Fills the given array with random numbers between the given min and
 *        max inclusive.
 * @param data the array to fill with random numbers
 * @param size the size of the given array
 * @param min the smallest random number to generate
 * @param max the largest random number to generate
 */
void fillArrayWithRandomData(int data[], int size, int min, int max);

/**
 * @brief Entry point for the Lab 3 demo.
 * @param argc the number of command line arguments; unused by this demo.
 * @param argv an array of command line arguments; unused by this demo.
 * @return EXIT_SUCCESS is returned upon successful completion of this function.
 */
int main(int argc, char **argv) {
    // Create and initialize an array of four integers. Since this is a local
    // variable, the memory allocated for these four integers resides in the
    // current stack frame.
    int intArray[] = {1, 2, 3, 4};

    // Create a pointer to an int. Since we're using new, this integer resides
    // in heap memory
    int *intPtr = new int;

    std::cout << "Printing values and addresses of array elements..."
              << std::endl;

    // Use a for-loop to iterate through the array
    for (array_index i = 0; i < 4; ++i) {
        printAddress(intArray[i]);
    }

    std::cout << std::endl;

    std::cout << "Printing value and address of a pointer..." << std::endl;
    // Notice how we dereference the pointer so that we're passing an int to
    // the function and not a pointer to an int
    printAddress(*intPtr);

    std::cout << std::endl;

    std::cout << "Printing values and address of pointers..." << std::endl;
    // Using pointer arithmetic, let's put new values into memory.
    // NOTE: Question 7a refers to this for-loop
    for (array_index i = 0; i < 4; ++i) {
        *(intPtr + i) = i + 1;
        printAddress(*(intPtr + i));
    }

    // Now that I'm done with this pointer, let's free the memory it pointed to
    // NOTE: Question 7b refers to this delete statement.
    delete intPtr;

    // Demonstrate the concept of a dynamic array. Normally, an array's size
    // must be stated as a constant, i.e., it can't be done using an unknown
    // that is determined at runtime. Dynamic arrays allow their size to be
    // specified during runtime.
    std::cout << "Enter a size between 1 and 10 followed by the [RETURN] key: ";
    int size;
    std::cin >> size;

    // NOTE: Question 8 refers to the following declaration
    int *dynamicArray = new int[size];

    fillArrayWithRandomData(dynamicArray, size, 0, 100);

    delete [] dynamicArray;

    return EXIT_SUCCESS;
}

void printAddress(int value) {
    std::cout << "Value: " << value
              << " is stored at address: " << &value
              << std::endl;
}

void fillArrayWithRandomData(int data[], int size, int min, int max) {
    // Seed with a real random value, if available
    std::random_device r;

    // Choose a random mean between min and max
    std::default_random_engine e1(r());
    std::uniform_int_distribution<int> uniform_dist(min, max);

    // Now let's fill this array with 0s...
    for (array_index i = 0; i < size; ++i) {
        // Notice I'm using a pointer (dynamicArray) with array-like syntax
        data[i] = uniform_dist(e1);
        std::cout << "dynamicArray[" << i << "] = " << data[i]
                  << std::endl;
    }
}
/*
 * TODO: Answer the following questions using grammatically correct,
 * complete sentences when instructed by the lab handout.
 *
 * 1. Why does the function printAddress(int value); always print the same
 *    address for every element in the array?
 *
 *    <Erase this line and put your answer here.>
 *
 * 2. After changing the function's signature, why does the function
 *    printAddress(const int& value); now print different addresses for every
 *    element in the array?
 *
 *    <Erase this line and put your answer here.>
 *
 * 3. From the given addresses now printed out using this new function
 *    signature, deduce how many bytes are occupied by integers.
 *
 *    <Erase this line and put your answer here.>
 *
 * 4. How could you modify this program to deduce how many bytes are occupied by
 *    long values?
 *
 *    <Erase this line and put your answer here.>
 *
 * 5. What are some similarities between pointers and arrays?
 *
 *    <Erase this line and put your answer here.>
 *
 * 6. What are some differences between pointers and arrays?
 *
 *    <Erase this line and put your answer here.>
 *
 * 7a. In the for loop that puts new values into memory using pointer
 *     arithmetic, why might this be a "dangerous" thing to do?
 *
 * 7b. Why do you suppose that I can't delete (intPtr + 1)?
 *
 *    <Erase these lines and put your answer here. The answers to these last
 *    two questions (8a + 8b) is really beyond the scope of this class, but I'm
 *    curious to see what you think... you will not be penalized for your
 *    answers to these last two questions.>
 *
 * 8. What error message do you get when you remove the "new" keyword used in
 *    declaring the dynamic array?
 *
 *    <Erase this line and put your answer here.>
 */
